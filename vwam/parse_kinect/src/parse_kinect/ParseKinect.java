/*
Author: Vladimir Serdyukov
Date: 2013-11-24
CS 156 project
Location: https://bitbucket.org/milieuleu/ai_vsifu_group3/src/898f82b5791f/parse_kinect/?at=master
Reference: http://json.org

To run: Use Eclipse (Kepler), passing parameter 1 the source folder with all the capture motion data in individual subfolders.
Parameter 2 is the output destination folder for all the parsed data.
*/
package parse_kinect;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class ParseKinect {

	static String sourceFolder; // pass in as 1st param
	static String destFolder; // pass in as 2nd param
	
	public static String readFile(String filename) throws IOException 
	{
	   String content = null;
	   File file = new File(filename); //for ex foo.txt
       FileReader reader = new FileReader(file);
       char[] chars = new char[(int) file.length()];
       reader.read(chars);
       content = new String(chars);
       reader.close();
	   return content;
	}
	
	public static void main(String[] args) throws IOException {
		
		String systemName = System.getProperty("os.name");
		
		sourceFolder = args[0];
		destFolder = args[1];
				
		File f = new File(sourceFolder);
		ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
		
		for (String name: names) {
			String targetFile = sourceFolder + (systemName.equals("Linux") ? "/" : "\\") + name;
			System.out.println("Processing " + name + "       " + targetFile);
			String jsonStr = readFile(targetFile);
	
	        JSONObject rootObject = new JSONObject(jsonStr); // Parse the JSON to a JSONObject
	        JSONArray motiondata = rootObject.getJSONArray("motiondata");
	        HashMap<String, String> joints = new HashMap<String, String>();
	
	        for(int i = 0; i < motiondata.length(); i++) {
	            JSONObject skeleton = motiondata.getJSONObject(i).getJSONObject("skeleton");
	            for(Object joint_index : skeleton.keySet()) { //int joint_index=0; joint_index < skeleton.length(); joint_index++) {
	                try {
	                	JSONObject joint =  skeleton.getJSONObject((String)joint_index);
	                    JSONArray position = joint.getJSONArray("position");
	                    JSONArray rotation = joint.getJSONArray("rotation");
	                    joints.put("J" + joint_index + "x", (joints.get("J" + joint_index + "x") == null ? "" : joints.get("J" + joint_index + "x") + " ") + position.get(0));
	                    joints.put("J" + joint_index + "y", (joints.get("J" + joint_index + "y") == null ? "" : joints.get("J" + joint_index + "y") + " ") + position.get(1));
	                    joints.put("J" + joint_index + "z", (joints.get("J" + joint_index + "z") == null ? "" : joints.get("J" + joint_index + "z") + " ") + position.get(2));
	                    for (int r = 0; r < rotation.length(); r++) {
	                    	joints.put("J" + joint_index + "R" + r, (joints.get("J" + joint_index + "R" + r) == null ? "" : joints.get("J" + joint_index + "R" + r) + " ") + rotation.get(r));
	                    }
	                }
	                catch (Exception e) {} //ignore
	            }
	        }
	        String filePath = destFolder + (systemName.equals("Linux") ? "/" : "\\") + name;
	        new File(filePath).mkdirs();
	        
	        for (String joint : joints.keySet()) {
	        	PrintWriter writer;
	        	if(systemName.equals("Linux")){
	        		writer = new PrintWriter(destFolder + "/" + name + "/" + joint + ".txt", "UTF-8");
	        	} else {
	        		writer = new PrintWriter(destFolder + "\\" + name + "\\" + joint + ".txt", "UTF-8");
	        	}
	        	writer.println(joints.get(joint));
	        	writer.close();
	        }
		}
		System.out.println("All done!");
	}

}