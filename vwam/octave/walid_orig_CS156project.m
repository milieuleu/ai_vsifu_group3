# workspace = 'C:\Users\242\Desktop\Walid\CS156\project\parsed\parsed';
workspace = '/home/milieu/group3/parsed/parsed';
workspacefilePattern = fullfile(workspace, '*.txt');
workspacefiles = dir(workspacefilePattern);

for workspacefile = workspacefiles'
     
studentFolder = fullfile(workspace, workspacefile.name);
studentfilePattern = fullfile(studentFolder, '*.txt');
studentfiles = dir(studentfilePattern);

# stevensFolder = 'C:\Users\242\Desktop\Walid\CS156\project\parsed\parsed\first_Steven-s1_u0.txt';
stevensFolder = '/home/milieu/group3/parsed/parsed/first_Steven-s1_u0.txt';
stevensfilePattern = fullfile(stevensFolder, '*.txt');
stevensfiles = dir(stevensfilePattern);

Output = [];
k=0;
for i=1:240
		stevensbaseFileName = stevensfiles(i).name; 
		stevensfullFileName = fullfile(stevensFolder, stevensbaseFileName);
		format long;
		
		t = load(stevensfullFileName);
		
		format long;
		
		studentbaseFileName = studentfiles(i).name;
		studentfullFileName = fullfile(studentFolder, studentbaseFileName);
		r = load(studentfullFileName);
		
		format long;
		[Dist]=dtw(t,r);
		Dist;
		Output = [Output, Dist];
		
		k = k + 1;
end

storepath = fullfile(studentFolder, 'Output.txt')
dlmwrite(storepath,Output);


end
