# workspace = 'C:\Users\Admin\Desktop\CS156\kinect\parsed\parsed';
workspace = '/home/milieu/group3/parsed';
workspacefilePattern = fullfile(workspace, '*.txt');
workspacefiles = dir(workspacefilePattern);
count = 0;
for workspacefile = workspacefiles'

studentFolder = fullfile(workspace, workspacefile.name)
studentfilePattern = fullfile(studentFolder, '*.txt');
studentfiles = dir(studentfilePattern);

# stevensFolder = 'C:\Users\Admin\Desktop\CS156\kinect\parsed\parsed\first_Steven-s1_u0.txt';
stevensFolder = '/home/milieu/group3/parsed/first_Steven-s1_u0.txt';
stevensfilePattern = fullfile(stevensFolder, '*.txt');
stevensfiles = dir(stevensfilePattern);

format long;

Output = [];
k=0;
for i=1:240
		stevensbaseFileName = stevensfiles(i).name; 
		stevensfullFileName = fullfile(stevensFolder, stevensbaseFileName);
		
		t = load(stevensfullFileName);
		t = (t .- min(t)) .* (100 / max(t) - min(t));
		
		
		studentbaseFileName = studentfiles(i).name;
		studentfullFileName = fullfile(studentFolder, studentbaseFileName);
		r = load(studentfullFileName);
		r = (r .- min(r)) .* (100 / max(r) - min(r));
		
		[Dist]=dtw(t,r);
		Dist;
		Output = [Output, Dist];
		
		k = k + 1;
		count = count + 1;
end

storepath = fullfile(studentFolder, 'Output.txt');
dlmwrite(storepath,Output);

count

end

