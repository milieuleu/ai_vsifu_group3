 fname = 'C_Curtis-s11_u0.txt';
 data=loadjson(fname);
 class(data)
 
 rows = size(data.motiondata)(2);
 
 %% Joint number 1
 J1X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_.position(1);
 J1X =[J1X, x];
 end
 class(J1X)
 J1X
 dlmwrite('J1X.txt',J1X);
 
 J1Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_.position(2);
 J1Y =[J1Y, y];
 end
 class(J1Y)
 J1Y
 dlmwrite('J1Y.txt',J1Y);
 
 
 J1Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_.position(3);
 J1Z =[J1Z, z];
 end
 class(J1Z)
 J1Z
 dlmwrite('J1Z.txt',J1Z);
 
 
 J1R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(1);
 J1R0 =[J1R0, r0];
 end
 class(J1R0)
 J1R0
 dlmwrite('J1R0.txt',J1R0);
 
 
 J1R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(2);
 J1R1 =[J1R1, r1];
 end
 class(J1R1)
 J1R1
 dlmwrite('J1R1.txt',J1R1);
 
 J1R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(3);
 J1R2 =[J1R2, r2];
 end
 class(J1R2)
 J1R2
 dlmwrite('J1R2.txt',J1R2);
 
 
 J1R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(4);
 J1R3 =[J1R3, r3];
 end
 class(J1R3)
 J1R3
 dlmwrite('J1R3.txt',J1R3);
 
  
 J1R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(5);
 J1R4 =[J1R4, r4];
 end
 class(J1R4)
 J1R4
 dlmwrite('J1R4.txt',J1R4);
 
 
 J1R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(6);
 J1R5 =[J1R5, r5];
 end
 class(J1R5)
 J1R5
 dlmwrite('J1R5.txt',J1R5);
 
 J1R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(7);
 J1R6 =[J1R6, r6];
 end
 class(J1R6)
 J1R6
 dlmwrite('J1R6.txt',J1R6);
 
 
 J1R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(8);
 J1R7 =[J1R7, r7];
 end
 class(J1R7)
 J1R7
 dlmwrite('J1R7.txt',J1R7);
 
 J1R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_.rotation(9);
 J1R8 =[J1R8, r8];
 end
 class(J1R8)
 J1R8
 dlmwrite('J1R8.txt',J1R8);
 
 
 
  %% Joint number 2
 J2X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x32_.position(1);
 J2X =[J2X, x];
 end
 class(J2X)
 J2X
 dlmwrite('J2X.txt',J2X);
 
 J2Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x32_.position(2);
 J2Y =[J2Y, y];
 end
 class(J2Y)
 J2Y
 dlmwrite('J2Y.txt',J2Y);
 
 
 J2Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x32_.position(3);
 J2Z =[J2Z, z];
 end
 class(J2Z)
 J2Z
 dlmwrite('J2Z.txt',J2Z);
 
 
 J2R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(1);
 J2R0 =[J2R0, r0];
 end
 class(J2R0)
 J2R0
 dlmwrite('J2R0.txt',J2R0);
 
 
 J2R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(2);
 J2R1 =[J2R1, r1];
 end
 class(J2R1)
 J2R1
 dlmwrite('J2R1.txt',J2R1);
 
 J2R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(3);
 J2R2 =[J2R2, r2];
 end
 class(J2R2)
 J2R2
 dlmwrite('J2R2.txt',J2R2);
 
 
 J2R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(4);
 J2R3 =[J2R3, r3];
 end
 class(J2R3)
 J2R3
 dlmwrite('J2R3.txt',J2R3);
 
  
 J2R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(5);
 J2R4 =[J2R4, r4];
 end
 class(J2R4)
 J2R4
 dlmwrite('J2R4.txt',J2R4);
 
 
 J2R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(6);
 J2R5 =[J2R5, r5];
 end
 class(J2R5)
 J2R5
 dlmwrite('J2R5.txt',J2R5);
 
 J2R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(7);
 J2R6 =[J2R6, r6];
 end
 class(J2R6)
 J2R6
 dlmwrite('J2R6.txt',J2R6);
 
 
 J2R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(8);
 J2R7 =[J2R7, r7];
 end
 class(J2R7)
 J2R7
 dlmwrite('J2R7.txt',J2R7);
 
 J2R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x32_.rotation(9);
 J2R8 =[J2R8, r8];
 end
 class(J2R8)
 J2R8
 dlmwrite('J2R8.txt',J2R8);
 
 
 
   %% Joint number 3
 J3X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x33_.position(1);
 J3X =[J3X, x];
 end
 class(J3X)
 J3X
 dlmwrite('J3X.txt',J3X);
 
 J3Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x33_.position(2);
 J3Y =[J3Y, y];
 end
 class(J3Y)
 J3Y
 dlmwrite('J3Y.txt',J3Y);
 
 
 J3Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x33_.position(3);
 J3Z =[J3Z, z];
 end
 class(J3Z)
 J3Z
 dlmwrite('J3Z.txt',J3Z);
 
 
 J3R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(1);
 J3R0 =[J3R0, r0];
 end
 class(J3R0)
 J3R0
 dlmwrite('J3R0.txt',J3R0);
 
 
 J3R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(2);
 J3R1 =[J3R1, r1];
 end
 class(J3R1)
 J3R1
 dlmwrite('J3R1.txt',J3R1);
 
 J3R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(3);
 J3R2 =[J3R2, r2];
 end
 class(J3R2)
 J3R2
 dlmwrite('J3R2.txt',J3R2);
 
 
 J3R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(4);
 J3R3 =[J3R3, r3];
 end
 class(J3R3)
 J3R3
 dlmwrite('J3R3.txt',J3R3);
 
  
 J3R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(5);
 J3R4 =[J3R4, r4];
 end
 class(J3R4)
 J3R4
 dlmwrite('J3R4.txt',J3R4);
 
 
 J3R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(6);
 J3R5 =[J3R5, r5];
 end
 class(J3R5)
 J3R5
 dlmwrite('J3R5.txt',J3R5);
 
 J3R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(7);
 J3R6 =[J3R6, r6];
 end
 class(J3R6)
 J3R6
 dlmwrite('J3R6.txt',J3R6);
 
 
 J3R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(8);
 J3R7 =[J3R7, r7];
 end
 class(J3R7)
 J3R7
 dlmwrite('J3R7.txt',J3R7);
 
 J3R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x33_.rotation(9);
 J3R8 =[J3R8, r8];
 end
 class(J3R8)
 J3R8
 dlmwrite('J3R8.txt',J3R8);
 
 
 
 
    %% Joint number 4
 J4X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x34_.position(1);
 J4X =[J4X, x];
 end
 class(J4X)
 J4X
 dlmwrite('J4X.txt',J4X);
 
 J4Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x34_.position(2);
 J4Y =[J4Y, y];
 end
 class(J4Y)
 J4Y
 dlmwrite('J4Y.txt',J4Y);
 
 
 J4Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x34_.position(3);
 J4Z =[J4Z, z];
 end
 class(J4Z)
 J4Z
 dlmwrite('J4Z.txt',J4Z);
 
 
 J4R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(1);
 J4R0 =[J4R0, r0];
 end
 class(J4R0)
 J4R0
 dlmwrite('J4R0.txt',J4R0);
 
 
 J4R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(2);
 J4R1 =[J4R1, r1];
 end
 class(J4R1)
 J4R1
 dlmwrite('J4R1.txt',J4R1);
 
 J4R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(3);
 J4R2 =[J4R2, r2];
 end
 class(J4R2)
 J4R2
 dlmwrite('J4R2.txt',J4R2);
 
 
 J4R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(4);
 J4R3 =[J4R3, r3];
 end
 class(J4R3)
 J4R3
 dlmwrite('J4R3.txt',J4R3);
 
  
 J4R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(5);
 J4R4 =[J4R4, r4];
 end
 class(J4R4)
 J4R4
 dlmwrite('J4R4.txt',J4R4);
 
 
 J4R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(6);
 J4R5 =[J4R5, r5];
 end
 class(J4R5)
 J4R5
 dlmwrite('J4R5.txt',J4R5);
 
 J4R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(7);
 J4R6 =[J4R6, r6];
 end
 class(J4R6)
 J4R6
 dlmwrite('J4R6.txt',J4R6);
 
 
 J4R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(8);
 J4R7 =[J4R7, r7];
 end
 class(J4R7)
 J4R7
 dlmwrite('J4R7.txt',J4R7);
 
 J4R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x34_.rotation(9);
 J4R8 =[J4R8, r8];
 end
 class(J4R8)
 J4R8
 dlmwrite('J4R8.txt',J4R8);
 
 
 
     %% Joint number 5
 J5X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x36_.position(1);
 J5X =[J5X, x];
 end
 class(J5X)
 J5X
 dlmwrite('J5X.txt',J5X);
 
 J5Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x36_.position(2);
 J5Y =[J5Y, y];
 end
 class(J5Y)
 J5Y
 dlmwrite('J5Y.txt',J5Y);
 
 
 J5Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x36_.position(3);
 J5Z =[J5Z, z];
 end
 class(J5Z)
 J5Z
 dlmwrite('J5Z.txt',J5Z);
 
 
 J5R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(1);
 J5R0 =[J5R0, r0];
 end
 class(J5R0)
 J5R0
 dlmwrite('J5R0.txt',J5R0);
 
 
 J5R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(2);
 J5R1 =[J5R1, r1];
 end
 class(J5R1)
 J5R1
 dlmwrite('J5R1.txt',J5R1);
 
 J5R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(3);
 J5R2 =[J5R2, r2];
 end
 class(J5R2)
 J5R2
 dlmwrite('J5R2.txt',J5R2);
 
 
 J5R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(4);
 J5R3 =[J5R3, r3];
 end
 class(J5R3)
 J5R3
 dlmwrite('J5R3.txt',J5R3);
 
  
 J5R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(5);
 J5R4 =[J5R4, r4];
 end
 class(J5R4)
 J5R4
 dlmwrite('J5R4.txt',J5R4);
 
 
 J5R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(6);
 J5R5 =[J5R5, r5];
 end
 class(J5R5)
 J5R5
 dlmwrite('J5R5.txt',J5R5);
 
 J5R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(7);
 J5R6 =[J5R6, r6];
 end
 class(J5R6)
 J5R6
 dlmwrite('J5R6.txt',J5R6);
 
 
 J5R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(8);
 J5R7 =[J5R7, r7];
 end
 class(J5R7)
 J5R7
 dlmwrite('J5R7.txt',J5R7);
 
 J5R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x36_.rotation(9);
 J5R8 =[J5R8, r8];
 end
 class(J5R8)
 J5R8
 dlmwrite('J5R8.txt',J5R8);
 
 
 
 
 
 
      %% Joint number 6
 J6X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x37_.position(1);
 J6X =[J6X, x];
 end
 class(J6X)
 J6X
 dlmwrite('J6X.txt',J6X);
 
 J6Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x37_.position(2);
 J6Y =[J6Y, y];
 end
 class(J6Y)
 J6Y
 dlmwrite('J6Y.txt',J6Y);
 
 
 J6Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x37_.position(3);
 J6Z =[J6Z, z];
 end
 class(J6Z)
 J6Z
 dlmwrite('J6Z.txt',J6Z);
 
 
 J6R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(1);
 J6R0 =[J6R0, r0];
 end
 class(J6R0)
 J6R0
 dlmwrite('J6R0.txt',J6R0);
 
 
 J6R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(2);
 J6R1 =[J6R1, r1];
 end
 class(J6R1)
 J6R1
 dlmwrite('J6R1.txt',J6R1);
 
 J6R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(3);
 J6R2 =[J6R2, r2];
 end
 class(J6R2)
 J6R2
 dlmwrite('J6R2.txt',J6R2);
 
 
 J6R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(4);
 J6R3 =[J6R3, r3];
 end
 class(J6R3)
 J6R3
 dlmwrite('J6R3.txt',J6R3);
 
  
 J6R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(5);
 J6R4 =[J6R4, r4];
 end
 class(J6R4)
 J6R4
 dlmwrite('J6R4.txt',J6R4);
 
 
 J6R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(6);
 J6R5 =[J6R5, r5];
 end
 class(J6R5)
 J6R5
 dlmwrite('J6R5.txt',J6R5);
 
 J6R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(7);
 J6R6 =[J6R6, r6];
 end
 class(J6R6)
 J6R6
 dlmwrite('J6R6.txt',J6R6);
 
 
 J6R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(8);
 J6R7 =[J6R7, r7];
 end
 class(J6R7)
 J6R7
 dlmwrite('J6R7.txt',J6R7);
 
 J6R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x37_.rotation(9);
 J6R8 =[J6R8, r8];
 end
 class(J6R8)
 J6R8
 dlmwrite('J6R8.txt',J6R8);
 
 
 
 
       %% Joint number 7
 J7X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x38_.position(1);
 J7X =[J7X, x];
 end
 class(J7X)
 J7X
 dlmwrite('J7X.txt',J7X);
 
 J7Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x38_.position(2);
 J7Y =[J7Y, y];
 end
 class(J7Y)
 J7Y
 dlmwrite('J7Y.txt',J7Y);
 
 
 J7Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x38_.position(3);
 J7Z =[J7Z, z];
 end
 class(J7Z)
 J7Z
 dlmwrite('J7Z.txt',J7Z);
 
 
 J7R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(1);
 J7R0 =[J7R0, r0];
 end
 class(J7R0)
 J7R0
 dlmwrite('J7R0.txt',J7R0);
 
 
 J7R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(2);
 J7R1 =[J7R1, r1];
 end
 class(J7R1)
 J7R1
 dlmwrite('J7R1.txt',J7R1);
 
 J7R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(3);
 J7R2 =[J7R2, r2];
 end
 class(J7R2)
 J7R2
 dlmwrite('J7R2.txt',J7R2);
 
 
 J7R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(4);
 J7R3 =[J7R3, r3];
 end
 class(J7R3)
 J7R3
 dlmwrite('J7R3.txt',J7R3);
 
  
 J7R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(5);
 J7R4 =[J7R4, r4];
 end
 class(J7R4)
 J7R4
 dlmwrite('J7R4.txt',J7R4);
 
 
 J7R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(6);
 J7R5 =[J7R5, r5];
 end
 class(J7R5)
 J7R5
 dlmwrite('J7R5.txt',J7R5);
 
 J7R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(7);
 J7R6 =[J7R6, r6];
 end
 class(J7R6)
 J7R6
 dlmwrite('J7R6.txt',J7R6);
 
 
 J7R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(8);
 J7R7 =[J7R7, r7];
 end
 class(J7R7)
 J7R7
 dlmwrite('J7R7.txt',J7R7);
 
 J7R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x38_.rotation(9);
 J7R8 =[J7R8, r8];
 end
 class(J7R8)
 J7R8
 dlmwrite('J7R8.txt',J7R8);
 
 
 
 
  %% Joint number 8
 J8X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x39_.position(1);
 J8X =[J8X, x];
 end
 class(J8X)
 J8X
 dlmwrite('J8X.txt',J8X);
 
 J8Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x39_.position(2);
 J8Y =[J8Y, y];
 end
 class(J8Y)
 J8Y
 dlmwrite('J8Y.txt',J8Y);
 
 
 J8Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x39_.position(3);
 J8Z =[J8Z, z];
 end
 class(J8Z)
 J8Z
 dlmwrite('J8Z.txt',J8Z);
 
 
 J8R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(1);
 J8R0 =[J8R0, r0];
 end
 class(J8R0)
 J8R0
 dlmwrite('J8R0.txt',J8R0);
 
 
 J8R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(2);
 J8R1 =[J8R1, r1];
 end
 class(J8R1)
 J8R1
 dlmwrite('J8R1.txt',J8R1);
 
 J8R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(3);
 J8R2 =[J8R2, r2];
 end
 class(J8R2)
 J8R2
 dlmwrite('J8R2.txt',J8R2);
 
 
 J8R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(4);
 J8R3 =[J8R3, r3];
 end
 class(J8R3)
 J8R3
 dlmwrite('J8R3.txt',J8R3);
 
  
 J8R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(5);
 J8R4 =[J8R4, r4];
 end
 class(J8R4)
 J8R4
 dlmwrite('J8R4.txt',J8R4);
 
 
 J8R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(6);
 J8R5 =[J8R5, r5];
 end
 class(J8R5)
 J8R5
 dlmwrite('J8R5.txt',J8R5);
 
 J8R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(7);
 J8R6 =[J8R6, r6];
 end
 class(J8R6)
 J8R6
 dlmwrite('J8R6.txt',J8R6);
 
 
 J8R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(8);
 J8R7 =[J8R7, r7];
 end
 class(J8R7)
 J8R7
 dlmwrite('J8R7.txt',J8R7);
 
 J8R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x39_.rotation(9);
 J8R8 =[J8R8, r8];
 end
 class(J8R8)
 J8R8
 dlmwrite('J8R8.txt',J8R8);
 
 
 
   %% Joint number 9
 J9X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_2.position(1);
 J9X =[J9X, x];
 end
 class(J9X)
 J9X
 dlmwrite('J9X.txt',J9X);
 
 J9Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_2.position(2);
 J9Y =[J9Y, y];
 end
 class(J9Y)
 J9Y
 dlmwrite('J9Y.txt',J9Y);
 
 
 J9Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_2.position(3);
 J9Z =[J9Z, z];
 end
 class(J9Z)
 J9Z
 dlmwrite('J9Z.txt',J9Z);
 
 
 J9R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(1);
 J9R0 =[J9R0, r0];
 end
 class(J9R0)
 J9R0
 dlmwrite('J9R0.txt',J9R0);
 
 
 J9R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(2);
 J9R1 =[J9R1, r1];
 end
 class(J9R1)
 J9R1
 dlmwrite('J9R1.txt',J9R1);
 
 J9R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(3);
 J9R2 =[J9R2, r2];
 end
 class(J9R2)
 J9R2
 dlmwrite('J9R2.txt',J9R2);
 
 
 J9R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(4);
 J9R3 =[J9R3, r3];
 end
 class(J9R3)
 J9R3
 dlmwrite('J9R3.txt',J9R3);
 
  
 J9R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(5);
 J9R4 =[J9R4, r4];
 end
 class(J9R4)
 J9R4
 dlmwrite('J9R4.txt',J9R4);
 
 
 J9R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(6);
 J9R5 =[J9R5, r5];
 end
 class(J9R5)
 J9R5
 dlmwrite('J9R5.txt',J9R5);
 
 J9R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(7);
 J9R6 =[J9R6, r6];
 end
 class(J9R6)
 J9R6
 dlmwrite('J9R6.txt',J9R6);
 
 
 J9R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(8);
 J9R7 =[J9R7, r7];
 end
 class(J9R7)
 J9R7
 dlmwrite('J9R7.txt',J9R7);
 
 J9R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_2.rotation(9);
 J9R8 =[J9R8, r8];
 end
 class(J9R8)
 J9R8
 dlmwrite('J9R8.txt',J9R8);
 
 
 
 
 
 
 
 %% Joint number 10
 J10X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_3.position(1);
 J10X =[J10X, x];
 end
 class(J10X)
 J10X
 dlmwrite('J10X.txt',J10X);
 
 J10Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_3.position(2);
 J10Y =[J10Y, y];
 end
 class(J10Y)
 J10Y
 dlmwrite('J10Y.txt',J10Y);
 
 
 J10Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_3.position(3);
 J10Z =[J10Z, z];
 end
 class(J10Z)
 J10Z
 dlmwrite('J10Z.txt',J10Z);
 
 
 J10R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(1);
 J10R0 =[J10R0, r0];
 end
 class(J10R0)
 J10R0
 dlmwrite('J10R0.txt',J10R0);
 
 
 J10R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(2);
 J10R1 =[J10R1, r1];
 end
 class(J10R1)
 J10R1
 dlmwrite('J10R1.txt',J10R1);
 
 J10R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(3);
 J10R2 =[J10R2, r2];
 end
 class(J10R2)
 J10R2
 dlmwrite('J10R2.txt',J10R2);
 
 
 J10R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(4);
 J10R3 =[J10R3, r3];
 end
 class(J10R3)
 J10R3
 dlmwrite('J10R3.txt',J10R3);
 
  
 J10R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(5);
 J10R4 =[J10R4, r4];
 end
 class(J10R4)
 J10R4
 dlmwrite('J10R4.txt',J10R4);
 
 
 J10R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(6);
 J10R5 =[J10R5, r5];
 end
 class(J10R5)
 J10R5
 dlmwrite('J10R5.txt',J10R5);
 
 J10R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(7);
 J10R6 =[J10R6, r6];
 end
 class(J10R6)
 J10R6
 dlmwrite('J10R6.txt',J10R6);
 
 
 J10R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(8);
 J10R7 =[J10R7, r7];
 end
 class(J10R7)
 J10R7
 dlmwrite('J10R7.txt',J10R7);
 
 J10R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_3.rotation(9);
 J10R8 =[J10R8, r8];
 end
 class(J10R8)
 J10R8
 dlmwrite('J10R8.txt',J10R8);
 
 
 
  %% Joint number 11
 J11X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_4.position(1);
 J11X =[J11X, x];
 end
 class(J11X)
 J11X
 dlmwrite('J11X.txt',J11X);
 
 J11Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_4.position(2);
 J11Y =[J11Y, y];
 end
 class(J11Y)
 J11Y
 dlmwrite('J11Y.txt',J11Y);
 
 
 J11Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_4.position(3);
 J11Z =[J11Z, z];
 end
 class(J11Z)
 J11Z
 dlmwrite('J11Z.txt',J11Z);
 
 
 J11R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(1);
 J11R0 =[J11R0, r0];
 end
 class(J11R0)
 J11R0
 dlmwrite('J11R0.txt',J11R0);
 
 
 J11R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(2);
 J11R1 =[J11R1, r1];
 end
 class(J11R1)
 J11R1
 dlmwrite('J11R1.txt',J11R1);
 
 J11R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(3);
 J11R2 =[J11R2, r2];
 end
 class(J11R2)
 J11R2
 dlmwrite('J11R2.txt',J11R2);
 
 
 J11R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(4);
 J11R3 =[J11R3, r3];
 end
 class(J11R3)
 J11R3
 dlmwrite('J11R3.txt',J11R3);
 
  
 J11R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(5);
 J11R4 =[J11R4, r4];
 end
 class(J11R4)
 J11R4
 dlmwrite('J11R4.txt',J11R4);
 
 
 J11R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(6);
 J11R5 =[J11R5, r5];
 end
 class(J11R5)
 J11R5
 dlmwrite('J11R5.txt',J11R5);
 
 J11R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(7);
 J11R6 =[J11R6, r6];
 end
 class(J11R6)
 J11R6
 dlmwrite('J11R6.txt',J11R6);
 
 
 J11R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(8);
 J11R7 =[J11R7, r7];
 end
 class(J11R7)
 J11R7
 dlmwrite('J11R7.txt',J11R7);
 
 J11R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_4.rotation(9);
 J11R8 =[J11R8, r8];
 end
 class(J11R8)
 J11R8
 dlmwrite('J11R8.txt',J11R8);
 
 
 
   %% Joint number 12
 J12X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_5.position(1);
 J12X =[J12X, x];
 end
 class(J12X)
 J12X
 dlmwrite('J12X.txt',J12X);
 
 J12Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_5.position(2);
 J12Y =[J12Y, y];
 end
 class(J12Y)
 J12Y
 dlmwrite('J12Y.txt',J12Y);
 
 
 J12Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_5.position(3);
 J12Z =[J12Z, z];
 end
 class(J12Z)
 J12Z
 dlmwrite('J12Z.txt',J12Z);
 
 
 J12R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(1);
 J12R0 =[J12R0, r0];
 end
 class(J12R0)
 J12R0
 dlmwrite('J12R0.txt',J12R0);
 
 
 J12R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(2);
 J12R1 =[J12R1, r1];
 end
 class(J12R1)
 J12R1
 dlmwrite('J12R1.txt',J12R1);
 
 J12R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(3);
 J12R2 =[J12R2, r2];
 end
 class(J12R2)
 J12R2
 dlmwrite('J12R2.txt',J12R2);
 
 
 J12R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(4);
 J12R3 =[J12R3, r3];
 end
 class(J12R3)
 J12R3
 dlmwrite('J12R3.txt',J12R3);
 
  
 J12R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(5);
 J12R4 =[J12R4, r4];
 end
 class(J12R4)
 J12R4
 dlmwrite('J12R4.txt',J12R4);
 
 
 J12R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(6);
 J12R5 =[J12R5, r5];
 end
 class(J12R5)
 J12R5
 dlmwrite('J12R5.txt',J12R5);
 
 J12R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(7);
 J12R6 =[J12R6, r6];
 end
 class(J12R6)
 J12R6
 dlmwrite('J12R6.txt',J12R6);
 
 
 J12R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(8);
 J12R7 =[J12R7, r7];
 end
 class(J12R7)
 J12R7
 dlmwrite('J12R7.txt',J12R7);
 
 J12R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_5.rotation(9);
 J12R8 =[J12R8, r8];
 end
 class(J12R8)
 J12R8
 dlmwrite('J12R8.txt',J12R8);
 
 
 
 
    %% Joint number 13
 J13X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_7.position(1);
 J13X =[J13X, x];
 end
 class(J13X)
 J13X
 dlmwrite('J13X.txt',J13X);
 
 J13Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_7.position(2);
 J13Y =[J13Y, y];
 end
 class(J13Y)
 J13Y
 dlmwrite('J13Y.txt',J13Y);
 
 
 J13Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_7.position(3);
 J13Z =[J13Z, z];
 end
 class(J13Z)
 J13Z
 dlmwrite('J13Z.txt',J13Z);
 
 
 J13R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(1);
 J13R0 =[J13R0, r0];
 end
 class(J13R0)
 J13R0
 dlmwrite('J13R0.txt',J13R0);
 
 
 J13R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(2);
 J13R1 =[J13R1, r1];
 end
 class(J13R1)
 J13R1
 dlmwrite('J13R1.txt',J13R1);
 
 J13R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(3);
 J13R2 =[J13R2, r2];
 end
 class(J13R2)
 J13R2
 dlmwrite('J13R2.txt',J13R2);
 
 
 J13R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(4);
 J13R3 =[J13R3, r3];
 end
 class(J13R3)
 J13R3
 dlmwrite('J13R3.txt',J13R3);
 
  
 J13R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(5);
 J13R4 =[J13R4, r4];
 end
 class(J13R4)
 J13R4
 dlmwrite('J13R4.txt',J13R4);
 
 
 J13R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(6);
 J13R5 =[J13R5, r5];
 end
 class(J13R5)
 J13R5
 dlmwrite('J13R5.txt',J13R5);
 
 J13R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(7);
 J13R6 =[J13R6, r6];
 end
 class(J13R6)
 J13R6
 dlmwrite('J13R6.txt',J13R6);
 
 
 J13R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(8);
 J13R7 =[J13R7, r7];
 end
 class(J13R7)
 J13R7
 dlmwrite('J13R7.txt',J13R7);
 
 J13R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_7.rotation(9);
 J13R8 =[J13R8, r8];
 end
 class(J13R8)
 J13R8
 dlmwrite('J13R8.txt',J13R8);
 
 
 
     %% Joint number 14
 J14X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_8.position(1);
 J14X =[J14X, x];
 end
 class(J14X)
 J14X
 dlmwrite('J14X.txt',J14X);
 
 J14Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_8.position(2);
 J14Y =[J14Y, y];
 end
 class(J14Y)
 J14Y
 dlmwrite('J14Y.txt',J14Y);
 
 
 J14Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_8.position(3);
 J14Z =[J14Z, z];
 end
 class(J14Z)
 J14Z
 dlmwrite('J14Z.txt',J14Z);
 
 
 J14R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(1);
 J14R0 =[J14R0, r0];
 end
 class(J14R0)
 J14R0
 dlmwrite('J14R0.txt',J14R0);
 
 
 J14R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(2);
 J14R1 =[J14R1, r1];
 end
 class(J14R1)
 J14R1
 dlmwrite('J14R1.txt',J14R1);
 
 J14R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(3);
 J14R2 =[J14R2, r2];
 end
 class(J14R2)
 J14R2
 dlmwrite('J14R2.txt',J14R2);
 
 
 J14R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(4);
 J14R3 =[J14R3, r3];
 end
 class(J14R3)
 J14R3
 dlmwrite('J14R3.txt',J14R3);
 
  
 J14R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(5);
 J14R4 =[J14R4, r4];
 end
 class(J14R4)
 J14R4
 dlmwrite('J14R4.txt',J14R4);
 
 
 J14R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(6);
 J14R5 =[J14R5, r5];
 end
 class(J14R5)
 J14R5
 dlmwrite('J14R5.txt',J14R5);
 
 J14R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(7);
 J14R6 =[J14R6, r6];
 end
 class(J14R6)
 J14R6
 dlmwrite('J14R6.txt',J14R6);
 
 
 J14R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(8);
 J14R7 =[J14R7, r7];
 end
 class(J14R7)
 J14R7
 dlmwrite('J14R7.txt',J14R7);
 
 J14R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_8.rotation(9);
 J14R8 =[J14R8, r8];
 end
 class(J14R8)
 J14R8
 dlmwrite('J14R8.txt',J14R8);
 
 
 
 
 
 
      %% Joint number 15
 J15X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x31_9.position(1);
 J15X =[J15X, x];
 end
 class(J15X)
 J15X
 dlmwrite('J15X.txt',J15X);
 
 J15Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x31_9.position(2);
 J15Y =[J15Y, y];
 end
 class(J15Y)
 J15Y
 dlmwrite('J15Y.txt',J15Y);
 
 
 J15Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x31_9.position(3);
 J15Z =[J15Z, z];
 end
 class(J15Z)
 J15Z
 dlmwrite('J15Z.txt',J15Z);
 
 
 J15R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(1);
 J15R0 =[J15R0, r0];
 end
 class(J15R0)
 J15R0
 dlmwrite('J15R0.txt',J15R0);
 
 
 J15R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(2);
 J15R1 =[J15R1, r1];
 end
 class(J15R1)
 J15R1
 dlmwrite('J15R1.txt',J15R1);
 
 J15R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(3);
 J15R2 =[J15R2, r2];
 end
 class(J15R2)
 J15R2
 dlmwrite('J15R2.txt',J15R2);
 
 
 J15R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(4);
 J15R3 =[J15R3, r3];
 end
 class(J15R3)
 J15R3
 dlmwrite('J15R3.txt',J15R3);
 
  
 J15R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(5);
 J15R4 =[J15R4, r4];
 end
 class(J15R4)
 J15R4
 dlmwrite('J15R4.txt',J15R4);
 
 
 J15R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(6);
 J15R5 =[J15R5, r5];
 end
 class(J15R5)
 J15R5
 dlmwrite('J15R5.txt',J15R5);
 
 J15R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(7);
 J15R6 =[J15R6, r6];
 end
 class(J15R6)
 J15R6
 dlmwrite('J15R6.txt',J15R6);
 
 
 J15R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(8);
 J15R7 =[J15R7, r7];
 end
 class(J15R7)
 J15R7
 dlmwrite('J15R7.txt',J15R7);
 
 J15R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x31_9.rotation(9);
 J15R8 =[J15R8, r8];
 end
 class(J15R8)
 J15R8
 dlmwrite('J15R8.txt',J15R8);
 
 
 
 
       %% Joint number 16
 J16X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x32_0.position(1);
 J16X =[J16X, x];
 end
 class(J16X)
 J16X
 dlmwrite('J16X.txt',J16X);
 
 J16Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x32_0.position(2);
 J16Y =[J16Y, y];
 end
 class(J16Y)
 J16Y
 dlmwrite('J16Y.txt',J16Y);
 
 
 J16Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x32_0.position(3);
 J16Z =[J16Z, z];
 end
 class(J16Z)
 J16Z
 dlmwrite('J16Z.txt',J16Z);
 
 
 J16R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(1);
 J16R0 =[J16R0, r0];
 end
 class(J16R0)
 J16R0
 dlmwrite('J16R0.txt',J16R0);
 
 
 J16R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(2);
 J16R1 =[J16R1, r1];
 end
 class(J16R1)
 J16R1
 dlmwrite('J16R1.txt',J16R1);
 
 J16R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(3);
 J16R2 =[J16R2, r2];
 end
 class(J16R2)
 J16R2
 dlmwrite('J16R2.txt',J16R2);
 
 
 J16R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(4);
 J16R3 =[J16R3, r3];
 end
 class(J16R3)
 J16R3
 dlmwrite('J16R3.txt',J16R3);
 
  
 J16R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(5);
 J16R4 =[J16R4, r4];
 end
 class(J16R4)
 J16R4
 dlmwrite('J16R4.txt',J16R4);
 
 
 J16R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(6);
 J16R5 =[J16R5, r5];
 end
 class(J16R5)
 J16R5
 dlmwrite('J16R5.txt',J16R5);
 
 J16R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(7);
 J16R6 =[J16R6, r6];
 end
 class(J16R6)
 J16R6
 dlmwrite('J16R6.txt',J16R6);
 
 
 J16R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(8);
 J16R7 =[J16R7, r7];
 end
 class(J16R7)
 J16R7
 dlmwrite('J16R7.txt',J16R7);
 
 J16R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x32_0.rotation(9);
 J16R8 =[J16R8, r8];
 end
 class(J16R8)
 J16R8
 dlmwrite('J16R8.txt',J16R8);
 
 
 
 
  %% Joint number 17
 J17X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x32_1.position(1);
 J17X =[J17X, x];
 end
 class(J17X)
 J17X
 dlmwrite('J17X.txt',J17X);
 
 J17Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x32_1.position(2);
 J17Y =[J17Y, y];
 end
 class(J17Y)
 J17Y
 dlmwrite('J17Y.txt',J17Y);
 
 
 J17Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x32_1.position(3);
 J17Z =[J17Z, z];
 end
 class(J17Z)
 J17Z
 dlmwrite('J17Z.txt',J17Z);
 
 
 J17R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(1);
 J17R0 =[J17R0, r0];
 end
 class(J17R0)
 J17R0
 dlmwrite('J17R0.txt',J17R0);
 
 
 J17R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(2);
 J17R1 =[J17R1, r1];
 end
 class(J17R1)
 J17R1
 dlmwrite('J17R1.txt',J17R1);
 
 J17R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(3);
 J17R2 =[J17R2, r2];
 end
 class(J17R2)
 J17R2
 dlmwrite('J17R2.txt',J17R2);
 
 
 J17R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(4);
 J17R3 =[J17R3, r3];
 end
 class(J17R3)
 J17R3
 dlmwrite('J17R3.txt',J17R3);
 
  
 J17R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(5);
 J17R4 =[J17R4, r4];
 end
 class(J17R4)
 J17R4
 dlmwrite('J17R4.txt',J17R4);
 
 
 J17R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(6);
 J17R5 =[J17R5, r5];
 end
 class(J17R5)
 J17R5
 dlmwrite('J17R5.txt',J17R5);
 
 J17R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(7);
 J17R6 =[J17R6, r6];
 end
 class(J17R6)
 J17R6
 dlmwrite('J17R6.txt',J17R6);
 
 
 J17R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(8);
 J17R7 =[J17R7, r7];
 end
 class(J17R7)
 J17R7
 dlmwrite('J17R7.txt',J17R7);
 
 J17R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x32_1.rotation(9);
 J17R8 =[J17R8, r8];
 end
 class(J17R8)
 J17R8
 dlmwrite('J17R8.txt',J17R8);
 
 
 
   %% Joint number 18
 J18X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x32_2.position(1);
 J18X =[J18X, x];
 end
 class(J18X)
 J18X
 dlmwrite('J18X.txt',J18X);
 
 J18Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x32_2.position(2);
 J18Y =[J18Y, y];
 end
 class(J18Y)
 J18Y
 dlmwrite('J18Y.txt',J18Y);
 
 
 J18Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x32_2.position(3);
 J18Z =[J18Z, z];
 end
 class(J18Z)
 J18Z
 dlmwrite('J18Z.txt',J18Z);
 
 
 J18R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(1);
 J18R0 =[J18R0, r0];
 end
 class(J18R0)
 J18R0
 dlmwrite('J18R0.txt',J18R0);
 
 
 J18R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(2);
 J18R1 =[J18R1, r1];
 end
 class(J18R1)
 J18R1
 dlmwrite('J18R1.txt',J18R1);
 
 J18R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(3);
 J18R2 =[J18R2, r2];
 end
 class(J18R2)
 J18R2
 dlmwrite('J18R2.txt',J18R2);
 
 
 J18R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(4);
 J18R3 =[J18R3, r3];
 end
 class(J18R3)
 J18R3
 dlmwrite('J18R3.txt',J18R3);
 
  
 J18R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(5);
 J18R4 =[J18R4, r4];
 end
 class(J18R4)
 J18R4
 dlmwrite('J18R4.txt',J18R4);
 
 
 J18R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(6);
 J18R5 =[J18R5, r5];
 end
 class(J18R5)
 J18R5
 dlmwrite('J18R5.txt',J18R5);
 
 J18R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(7);
 J18R6 =[J18R6, r6];
 end
 class(J18R6)
 J18R6
 dlmwrite('J18R6.txt',J18R6);
 
 
 J18R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(8);
 J18R7 =[J18R7, r7];
 end
 class(J18R7)
 J18R7
 dlmwrite('J18R7.txt',J18R7);
 
 J18R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x32_2.rotation(9);
 J18R8 =[J18R8, r8];
 end
 class(J18R8)
 J18R8
 dlmwrite('J18R8.txt',J18R8);
 
 
 
 
    %% Joint number 19
 J19X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x32_3.position(1);
 J19X =[J19X, x];
 end
 class(J19X)
 J19X
 dlmwrite('J19X.txt',J19X);
 
 J19Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x32_3.position(2);
 J19Y =[J19Y, y];
 end
 class(J19Y)
 J19Y
 dlmwrite('J19Y.txt',J19Y);
 
 
 J19Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x32_3.position(3);
 J19Z =[J19Z, z];
 end
 class(J19Z)
 J19Z
 dlmwrite('J19Z.txt',J19Z);
 
 
 J19R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(1);
 J19R0 =[J19R0, r0];
 end
 class(J19R0)
 J19R0
 dlmwrite('J19R0.txt',J19R0);
 
 
 J19R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(2);
 J19R1 =[J19R1, r1];
 end
 class(J19R1)
 J19R1
 dlmwrite('J19R1.txt',J19R1);
 
 J19R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(3);
 J19R2 =[J19R2, r2];
 end
 class(J19R2)
 J19R2
 dlmwrite('J19R2.txt',J19R2);
 
 
 J19R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(4);
 J19R3 =[J19R3, r3];
 end
 class(J19R3)
 J19R3
 dlmwrite('J19R3.txt',J19R3);
 
  
 J19R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(5);
 J19R4 =[J19R4, r4];
 end
 class(J19R4)
 J19R4
 dlmwrite('J19R4.txt',J19R4);
 
 
 J19R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(6);
 J19R5 =[J19R5, r5];
 end
 class(J19R5)
 J19R5
 dlmwrite('J19R5.txt',J19R5);
 
 J19R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(7);
 J19R6 =[J19R6, r6];
 end
 class(J19R6)
 J19R6
 dlmwrite('J19R6.txt',J19R6);
 
 
 J19R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(8);
 J19R7 =[J19R7, r7];
 end
 class(J19R7)
 J19R7
 dlmwrite('J19R7.txt',J19R7);
 
 J19R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x32_3.rotation(9);
 J19R8 =[J19R8, r8];
 end
 class(J19R8)
 J19R8
 dlmwrite('J19R8.txt',J19R8);
 
 
 
 
     %% Joint number 20
 J20X = [];
 for i=1:rows
 x = data.motiondata{1,i}.skeleton(1,1).x0x32_4.position(1);
 J20X =[J20X, x];
 end
 class(J20X)
 J20X
 dlmwrite('J20X.txt',J20X);
 
 J20Y = [];
 for i=1:rows
 y = data.motiondata{1,i}.skeleton(1,1).x0x32_4.position(2);
 J20Y =[J20Y, y];
 end
 class(J20Y)
 J20Y
 dlmwrite('J20Y.txt',J20Y);
 
 
 J20Z = [];
 for i=1:rows
 z = data.motiondata{1,i}.skeleton(1,1).x0x32_4.position(3);
 J20Z =[J20Z, z];
 end
 class(J20Z)
 J20Z
 dlmwrite('J20Z.txt',J20Z);
 
 
 J20R0 = [];
 for i=1:rows
 r0 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(1);
 J20R0 =[J20R0, r0];
 end
 class(J20R0)
 J20R0
 dlmwrite('J20R0.txt',J20R0);
 
 
 J20R1 = [];
 for i=1:rows
 r1 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(2);
 J20R1 =[J20R1, r1];
 end
 class(J20R1)
 J20R1
 dlmwrite('J20R1.txt',J20R1);
 
 J20R2 = [];
 for i=1:rows
 r2 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(3);
 J20R2 =[J20R2, r2];
 end
 class(J20R2)
 J20R2
 dlmwrite('J20R2.txt',J20R2);
 
 
 J20R3 = [];
 for i=1:rows
 r3 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(4);
 J20R3 =[J20R3, r3];
 end
 class(J20R3)
 J20R3
 dlmwrite('J20R3.txt',J20R3);
 
  
 J20R4 = [];
 for i=1:rows
 r4 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(5);
 J20R4 =[J20R4, r4];
 end
 class(J20R4)
 J20R4
 dlmwrite('J20R4.txt',J20R4);
 
 
 J20R5 = [];
 for i=1:rows
 r5 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(6);
 J20R5 =[J20R5, r5];
 end
 class(J20R5)
 J20R5
 dlmwrite('J20R5.txt',J20R5);
 
 J20R6 = [];
 for i=1:rows
 r6 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(7);
 J20R6 =[J20R6, r6];
 end
 class(J20R6)
 J20R6
 dlmwrite('J20R6.txt',J20R6);
 
 
 J20R7 = [];
 for i=1:rows
 r7 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(8);
 J20R7 =[J20R7, r7];
 end
 class(J20R7)
 J20R7
 dlmwrite('J20R7.txt',J20R7);
 
 J20R8 = [];
 for i=1:rows
 r8 = data.motiondata{1,i}.skeleton(1,1).x0x32_4.rotation(9);
 J20R8 =[J20R8, r8];
 end
 class(J20R8)
 J20R8
 dlmwrite('J20R8.txt',J20R8);